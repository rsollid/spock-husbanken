package pubsub;

public interface Subscriber {
    void receive(String event);
}

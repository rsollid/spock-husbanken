package pubsub;

import java.util.ArrayList;
import java.util.List;

public class Publisher {
    List<Subscriber> subscribers = new ArrayList<>();

    public void sendEvent(final String event) {
        subscribers.forEach( s -> {
            try {
                s.receive(event);
            } catch (Exception ignored) {
            }
        });
    }
}

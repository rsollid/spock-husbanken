package state;

import java.math.BigDecimal;

public class NegativeAmountWithdrawnException extends RuntimeException {
    private final BigDecimal amount;

    public NegativeAmountWithdrawnException(BigDecimal amount) {
        super("cannot uttak " + amount);
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

}

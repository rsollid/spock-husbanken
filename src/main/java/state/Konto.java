package state;

import java.math.BigDecimal;

public class Konto {
    private BigDecimal saldo = new BigDecimal(0);

    public Konto(BigDecimal initial) {
        saldo = initial;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void uttak(BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new NegativeAmountWithdrawnException(amount);
        }
        saldo = saldo.subtract(amount);
    }
}

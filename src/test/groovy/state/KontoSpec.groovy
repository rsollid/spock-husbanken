package state

import spock.lang.Specification
import spock.lang.Unroll

class KontoSpec extends Specification {
    @Unroll
    def "uttak #uttak fra konto med saldo #saldo"() {
        given: "En ny konto med saldo #saldo"
        def konto = new Konto(saldo)

        when: "vi tar ut #uttak"
        konto.uttak(uttak)

        then: "saldo skal være #nysaldo"
        konto.saldo == nysaldo //old(konto.saldo) - uttak

        where:
        saldo | uttak || nysaldo
        5.0   | 2.0   ||  3.0
        4.0   | 1.0   ||  3.0
        4.0   | 4.0   ||  0.0
    }
}

package domain

import spock.lang.Specification

class PersonSpec extends Specification {
    def "Test person" () {
        given: "Some people"
        def person = new Person(name: "Johan")
        def person2 = new Person(name: "Kari", age: 22)

        expect:
        person.age == 0
        person2.age == 22
        person.name == "Johan"
    }
}

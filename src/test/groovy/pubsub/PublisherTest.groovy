package pubsub

import spock.lang.Specification

class PublisherSpec extends Specification {
    def pub = new Publisher()
    def sub1 = Mock(Subscriber)
    def sub2 = Mock(Subscriber)

    def setup() {
        pub.subscribers << sub1 << sub2
    }

    def "delivers evnets to all subscribers" () {
        when:
        pub.sendEvent("event")

        then:
        1 * sub1.receive("event")
        1 * sub2.receive("event")
    }

    def "can cope with missbehaving subscribers"() {
        sub1.receive(_) >> { throw new Exception()}

        when:
        pub.sendEvent("event1")
        pub.sendEvent("event2")

        then:
        1 * sub2.receive("event1")
        1 * sub2.receive("event2")
    }
}
